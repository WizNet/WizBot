﻿namespace WizBot.Modules.Games;

public enum SetPixelResult
{
    Success,
    InsufficientPayment,
    NotEnoughMoney,
    InvalidInput
}